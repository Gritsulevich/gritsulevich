from django.contrib import admin
from .models import Record, User, Worker, Work, Calendar, Company


@admin.register(Record)
class RecordAdmin(admin.ModelAdmin):
    list_display = ['id', 'chat_id', 'work',  'start_time', 'end_time', 'day', 'company']
    ordering = ['-id']
    from_encoding = 'utf-8'
    search_fields = ['chat_id', 'work', 'start_time', 'day', ]
    # actions = [availability_true, availability_false, not_available, ]
    # resource_class = FuelInfoResource
    list_editable = ['start_time', 'end_time', 'day']


@admin.register(Calendar)
class CalendarAdmin(admin.ModelAdmin):
    list_display = ['day']
    ordering = ['day']
    from_encoding = 'utf-8'
    search_fields = ['day']


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ['name']
    ordering = ['name']
    from_encoding = 'utf-8'
    search_fields = ['name']


@admin.register(Worker)
class WorkerAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'phone_number', 'chat_id', 'company']
    ordering = ['-id']
    from_encoding = 'utf-8'
    search_fields = ['first_name', 'last_name', 'phone_number', 'chat_id',]
    # actions = [availability_true, availability_false, not_available, ]
    # resource_class = FuelInfoResource
    # list_editable = ['start_time', 'end_time', 'day']


@admin.register(Work)
class WorkAdmin(admin.ModelAdmin):
    list_display = ['name', 'descriptions', 'time', 'price', 'company']
    ordering = ['-id']
    from_encoding = 'utf-8'
    search_fields = ['name', 'descriptions', 'time', 'price', 'company']


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['name', 'user_name', 'status', 'chat_id', 'hapy_day', 'yer', 'phone_number', 'company']
    ordering = ['name']
    from_encoding = 'utf-8'
    search_fields = ['name', 'chat_id', 'phone_number', 'yer', 'user_name']
    list_editable = ['status']
