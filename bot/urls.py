
from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt
from .views import TutorialBotView, RegisterBotView, dashboard, worker, workers, work, notifications, calendar, login, \
    update_workers, create_record, get_inf, company, works, up_work, up_record, find, delete_record, exsport_users, \
    users, new_record, api, record, user_detail, get_free_time, analytic

urlpatterns = [
    path('company_bot/<str:token>', csrf_exempt(TutorialBotView.as_view())),
    path('register/<str:token>', csrf_exempt(RegisterBotView.as_view())),
    path('worker', worker, name='worker'),
    path('get_inf', get_inf, name='get_inf'),
    path('create_record/<str:day>/<int:time>/<int:worker>/', create_record, name='create_record'),
    path('workers', workers, name='workers'),
    path('work', work, name='work'),
    path('up_work/<int:id>', up_work, name='up_work'),
    path('up_record/<int:id>', up_record, name='up_record'),
    path('new_record/', new_record, name='new_record'),
    path('works', works, name='works'),
    path('api', api),
    path('notifications', notifications, name='notifications'),
    path('calendar', calendar, name='calendar'),
    path('company/<str:name>', company, name='company'),
    path('record/<str:name>', record, name='record'),
    path('get_free_time/<str:name>/<int:id>', get_free_time, name='get_free_time'),
    path('dashboard', dashboard, name='dashboard'),
    path('login', login, name='login'),
    path('delete_record', delete_record, name='delete_record'),
    # path('find', find, name='find'),
    path('user_card/<int:id>', user_detail, name='user_detail'),
    path('users', users, name='users'),
    path('analytic', analytic, name='analytic'),
    # path('call', call, name='call'),
    path('export/users', exsport_users, name='export_users'),
    path('update_workers', update_workers, name='update_workers'),


]
urlpatterns += [
    path('accounts/', include('django.contrib.auth.urls')),
]