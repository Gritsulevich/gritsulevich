# Generated by Django 3.2.9 on 2022-10-16 13:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0006_auto_20221016_1558'),
    ]

    operations = [
        migrations.AlterField(
            model_name='record',
            name='chat_id',
            field=models.IntegerField(blank=True),
        ),
    ]
