# Generated by Django 3.2.9 on 2022-11-12 19:03

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0012_auto_20221016_2126'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('image', models.TextField()),
                ('price', models.IntegerField(blank=True)),
                ('descriptions', models.TextField()),
                ('company', models.CharField(max_length=200)),
                ('available', models.BooleanField(db_index=True, default=True)),
            ],
        ),
        migrations.AddField(
            model_name='record',
            name='comment',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='record',
            name='created_day',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='record',
            name='price',
            field=models.IntegerField(blank=True, default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='record',
            name='updated_day',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='user',
            name='address',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='comment',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='image',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='record',
            name='day',
            field=models.DateTimeField(),
        ),
    ]
