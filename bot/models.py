from django.db import models


class User(models.Model):
    chat_id = models.CharField(max_length=20, blank=True, null=True)
    company = models.CharField(max_length=200, db_index=True)
    name = models.CharField(max_length=200, blank=True)
    user_name = models.CharField(max_length=200, blank=True, null=True)
    yer = models.CharField(max_length=5, blank=True)
    hapy_day = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=50, blank=True)
    phone_number = models.CharField(max_length=14, blank=True)
    available = models.BooleanField(default=True, db_index=True)
    comment = models.TextField(blank=True, null=True)
    image = models.TextField(blank=True)
    address = models.TextField(blank=True)

    class Meta:
        unique_together = ['chat_id', 'phone_number']

    def __str__(self):
        return str(self.name)


class PhoneNumber(models.Model):
    chat_id = models.IntegerField(blank=True)
    phone_number = models.CharField(max_length=14)
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class Product(models.Model):
    name = models.CharField(max_length=200)
    image = models.TextField()
    price = models.IntegerField(blank=True)
    descriptions = models.TextField()
    company = models.CharField(max_length=200)
    available = models.BooleanField(default=True, db_index=True)

    def __str__(self):
        return str(self.name)


class Calendar(models.Model):
    day = models.CharField(max_length=12)
    date = models.DateTimeField(db_index=True, blank=True, null=True)


class Worker(models.Model):
    first_name = models.CharField(max_length=200, blank=True)
    last_name = models.CharField(max_length=200, blank=True)
    user_name = models.CharField(max_length=200, blank=True)
    img = models.CharField(max_length=500, blank=True, null=True)
    hapy_day = models.DateTimeField(blank=True)
    chat_id = models.IntegerField(blank=True)
    phone_number = models.CharField(max_length=14)
    company = models.CharField(max_length=200, db_index=True)
    day = models.ManyToManyField(Calendar, blank=True)
    start_work = models.IntegerField(blank=True, default=20)
    end_work = models.IntegerField(blank=True, default=40)
    available = models.BooleanField(default=True, db_index=True)

    class Meta:
        unique_together = ('chat_id', 'user_name')

    def __str__(self):
        return str(self.first_name)


class Work(models.Model):
    name = models.CharField(max_length=200, blank=True)
    descriptions = models.TextField()
    time = models.IntegerField(blank=True)
    img = models.TextField()
    price = models.IntegerField(blank=True)
    worker = models.ManyToManyField(Worker)
    company = models.CharField(max_length=200, db_index=True)
    available = models.BooleanField(default=True, db_index=True)
    interval = models.IntegerField(blank=True)


# class Task(models.Model):
#     name = models.CharField(max_length=200, blank=True)
#     descriptions = models.TextField()
#     date = models.DateTimeField(db_index=True)
#     created_date = models.DateTimeField(db_index=True, auto_now_add=True)
#     updated_date = models.DateTimeField(auto_now=True)
#     status = models.CharField(max_length=200, blank=True)
#     available = models.BooleanField(default=False, db_index=True)
#     company = models.CharField(max_length=200, db_index=True)
#     user =


class Record(models.Model):
    company = models.CharField(max_length=200, db_index=True)
    work = models.ForeignKey(Work, on_delete=models.CASCADE)
    start_time = models.IntegerField(db_index=True)
    end_time = models.IntegerField()
    day = models.DateTimeField(db_index=True)
    created_day = models.DateTimeField(db_index=True, auto_now_add=True)
    updated_day = models.DateTimeField(auto_now=True)
    chat_id = models.IntegerField(blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    worker = models.ForeignKey(Worker, on_delete=models.CASCADE)
    available = models.BooleanField(default=True, db_index=True)
    notification_day = models.BooleanField(default=False, db_index=True)
    notification_hour = models.BooleanField(default=False, db_index=True)
    price = models.IntegerField(blank=True, null=True)
    comment = models.TextField(blank=True)

    # language = models.ForeignKey('Language', on_delete=models.SET_NULL, null=True)
    class Meta:
        ordering = ['day', 'start_time']

    def __str__(self):
        return str(self.user)


class Company(models.Model):
    name = models.CharField(max_length=200, db_index=True)
    google_name = models.CharField(max_length=200)
    descriptions = models.TextField(blank=True)
    keywords = models.CharField(max_length=200, blank=True)
    logo = models.CharField(max_length=200, blank=True)
    banner = models.CharField(max_length=200, blank=True)
    mini_descriptions = models.TextField(blank=True)
    link_star = models.CharField(max_length=200, blank=True)
    cade_1 = models.TextField(blank=True)
    code_2 = models.TextField(blank=True)
    code_3 = models.TextField(blank=True)
    phone_number = models.CharField(max_length=15, blank=True)
    phone_number2 = models.CharField(max_length=15, blank=True)
    instagram = models.CharField(max_length=30, blank=True)
    url_bot = models.CharField(max_length=200, blank=True)
    fb = models.CharField(max_length=200, blank=True)
    tiktok = models.CharField(max_length=200, blank=True)
    footer = models.TextField(blank=True)
    address = models.CharField(max_length=200, blank=True)
    policy = models.TextField(blank=True)
    link_address = models.TextField(blank=True)
    work_day = models.TextField(blank=True)
    about = models.TextField(blank=True)
    contact_page = models.TextField(blank=True)
    email = models.CharField(max_length=100, blank=True)
    theme = models.IntegerField(blank=True)
    available = models.BooleanField(default=True, blank=True, db_index=True)
    available_2 = models.BooleanField(default=True, blank=True, db_index=True)
    available_3 = models.BooleanField(default=True, blank=True, db_index=True)
    company = models.CharField(max_length=200, blank=True, db_index=True)
    chat_id = models.CharField(max_length=20, blank=True, null=True)
    views = models.IntegerField(default=0, db_index=True)
    check_number = models.IntegerField(default=0)
    check_bot = models.IntegerField(default=0)
    check_social = models.IntegerField(default=0)
    check_star = models.IntegerField(default=0)
    views_portal = models.IntegerField(default=0)
    from_bot = models.IntegerField(default=0)