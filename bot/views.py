import asyncio
import csv
import datetime
from time import sleep

from django.db.models import Q
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect

from .tasks import create_random_user_accounts
from django.forms import model_to_dict
from django.shortcuts import render, redirect

# celery worker --app=core --loglevel=info --logfile=logs/celery.log

import xlwt

import json
import os
import sqlite3

import requests
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.views import View
from telebot import types
from telegram_bot_calendar import DetailedTelegramCalendar, LSTEP
from .models import Record, Worker, Work, User, Calendar, Company

# from .models import tb_tutorial_collection


# https://5779-109-108-245-24.eu.ngrok.io
# ngrok http 127.0.0.1:80
# https://api.telegram.org/bot1083489163:AAGrgD1y0YleqA9oftda3W8YXEHUW9_RGy0/setWebhook?url=https://4c7c-109-108-245-24.eu.ngrok.io/webhooks/tutorial/
TELEGRAM_URL = "https://api.telegram.org/bot"
TUTORIAL_BOT_TOKEN = '1083489163:AAGrgD1y0YleqA9oftda3W8YXEHUW9_RGy0'


# answer ми відправили смс і включили стату опитування, відповів, ми поставили статус опитування false опитування завершено

# commands
# b'{"update_id":968143301,\n"message":{"message_id":5785,"from":{"id":537238295,"is_bot":false,"first_name":"\\u0412\\u0456\\u0442\\u0430\\u043b\\u0456\\u0439","username":"gritsulevich","language_code":"en"},"chat":{"id":537238295,"f
# irst_name":"\\u0412\\u0456\\u0442\\u0430\\u043b\\u0456\\u0439","username":"gritsulevich","type":"private"},"date":1663869049,"text":"/start","entities":[{"offset":0,"length":6,"type":"bot_command"}]}}'


# callback
# b'{"update_id":968143302,\n"callback_query":{"id":"2307420909198120741","from":{"id":537238295,"is_bot":false,"first_name":"\\u0412\\u0456\\u0442\\u0430\\u043b\\u0456\\u0439","username":"gritsulevich","language_code":"en"},"message"
# :{"message_id":5786,"from":{"id":1083489163,"is_bot":true,"first_name":"\\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d \\u043c\\u043e\\u0439","username":"my_funyshop_bot"},"chat":{"id":537238295,"first_name":"\\u0412\\u0456\\u044
# 2\\u0430\\u043b\\u0456\\u0439","username":"gritsulevich","type":"private"},"date":1663869049,"text":"hello 2","reply_markup":{"inline_keyboard":[[{"text":"\\u0417\\u0430\\u043f\\u0438\\u0441\\u0430\\u0442\\u0438\\u0441\\u044c","call
# back_data":"cbcal_0_s_y_2022_9_12"}]]}},"chat_instance":"-6439604943284597896","data":"cbcal_0_s_y_2022_9_12"}}'


# message
# b'{"update_id":968143307,\n"message":{"message_id":5795,"from":{"id":537238295,"is_bot":false,"first_name":"\\u0412\\u0456\\u0442\\u0430\\u043b\\u0456\\u0439","username":"gritsulevich","language_code":"en"},"chat":{"id":537238295,"f
# irst_name":"\\u0412\\u0456\\u0442\\u0430\\u043b\\u0456\\u0439","username":"gritsulevich","type":"private"},"date":1663869202,"text":"hg"}}'

# smile

# b'{"update_id":968143310,\n"message":{"message_id":5799,"from":{"id":537238295,"is_bot":false,"first_name":"\\u0412\\u0456\\u0442\\u0430\\u043b\\u0456\\u0439","username":"gritsulevich","language_code":"en"},"chat":{"id":537238295,"f
# irst_name":"\\u0412\\u0456\\u0442\\u0430\\u043b\\u0456\\u0439","username":"gritsulevich","type":"private"},"date":1663871425,"text":"\\ud83d\\ude03"}}'

# image
# b'{"update_id":968143311,\n"message":{"message_id":5800,"from":{"id":537238295,"is_bot":false,"first_name":"\\u0412\\u0456\\u0442\\u0430\\u043b\\u0456\\u0439","username":"gritsulevich","language_code":"en"},"chat":{"id":537238295,"f
# irst_name":"\\u0412\\u0456\\u0442\\u0430\\u043b\\u0456\\u0439","username":"gritsulevich","type":"private"},"date":1663871478,"photo":[{"file_id":"AgACAgIAAxkBAAIWqGMsqfXZHwINymakeE3aB1GIfwO9AAIZwjEb5iBpSaGwbbT412VfAQADAgADcwADKQQ","
# file_unique_id":"AQADGcIxG-YgaUl4","file_size":830,"width":90,"height":57},{"file_id":"AgACAgIAAxkBAAIWqGMsqfXZHwINymakeE3aB1GIfwO9AAIZwjEb5iBpSaGwbbT412VfAQADAgADbQADKQQ","file_unique_id":"AQADGcIxG-YgaUly","file_size":7096,"width"
# :320,"height":204},{"file_id":"AgACAgIAAxkBAAIWqGMsqfXZHwINymakeE3aB1GIfwO9AAIZwjEb5iBpSaGwbbT412VfAQADAgADeAADKQQ","file_unique_id":"AQADGcIxG-YgaUl9","file_size":26679,"width":800,"height":509},{"file_id":"AgACAgIAAxkBAAIWqGMsqfXZ
# HwINymakeE3aB1GIfwO9AAIZwjEb5iBpSaGwbbT412VfAQADAgADeQADKQQ","file_unique_id":"AQADGcIxG-YgaUl-","file_size":46584,"width":1100,"height":700}]}}'

# voice
# b'{"update_id":968143312,\n"message":{"message_id":5801,"from":{"id":537238295,"is_bot":false,"first_name":"\\u0412\\u0456\\u0442\\u0430\\u043b\\u0456\\u0439","username":"gritsulevich","language_code":"en"},"chat":{"id":537238295,"f
# irst_name":"\\u0412\\u0456\\u0442\\u0430\\u043b\\u0456\\u0439","username":"gritsulevich","type":"private"},"date":1663871574,"voice":{"duration":6,"mime_type":"audio/ogg","file_id":"AwACAgIAAxkBAAIWqWMsqlb3voExGufb0Re27ysI1-cVAAKnIQ
# AC5iBpSS353d9RQvwYKQQ","file_unique_id":"AgADpyEAAuYgaUk","file_size":40679}}}'

# file
# b'{"update_id":968143313,\n"message":{"message_id":5802,"from":{"id":537238295,"is_bot":false,"first_name":"\\u0412\\u0456\\u0442\\u0430\\u043b\\u0456\\u0439","username":"gritsulevich","language_code":"en"},"chat":{"id":537238295,"f
# irst_name":"\\u0412\\u0456\\u0442\\u0430\\u043b\\u0456\\u0439","username":"gritsulevich","type":"private"},"date":1663871641,"document":{"file_name":"R.txt","mime_type":"text/plain","file_id":"BQACAgIAAxkBAAIWqmMsqpn6K26Vq7Uikl8uuG5
# 69PgEAAKvIQAC5iBpSXbijX_2Niv2KQQ","file_unique_id":"AgADryEAAuYgaUk","file_size":1724}}}'

# message_type = json.loads(request.body)['message']['text'] # message / smile / command
# message_type = json.loads(request.body)['callback_query'] # callback
# message_type = json.loads(request.body)['message']['file_size'] # file



pause = 30
close_hours = []
next_step_user = {}

def login(request):
    return render(request, 'registration/login.html')


def analytic(request):
    return render(request, 'analytic.html')


def company(request, name):
    if request.method == "POST":
        company = Company.objects.get(id=request.POST.get('id'))
        data = {
            "chat_id": company.chat_id,
            "text": f"""*Заявка з сайту:* \nІм'я: *{request.POST.get("name")}*\nМобільний: *{request.POST.get("phone")}*\n{request.POST.get("text")}""",
            "parse_mode": "Markdown",
        }
        response = requests.post(
            f"{TELEGRAM_URL}{company.company}/sendMessage", data=data
        )
        print(response.json())


    company_page = Company.objects.get(google_name=name)
    service = Work.objects.all()[:3]
    return render(request, 'company/index.html', {'company': company_page, 'service': service})


def record(request, name):
    if request.method == "POST":
        data = request.POST
        work = Work.objects.get(id=int(data.get('work')))
        worker = Worker.objects.get(id=int(data.get('worker')))
        company = Company.objects.get(google_name=name)

        mess = {
            "chat_id": '537238295',
            "text": f"""*Новий запис:* \nІм'я: *{data.get("name")}*\nМобільний: *{data.get("phone")}*\n{work.name} - {work.price}\n{data.get('date')}: {data.get('time')} - {create_time(create_system_time(data.get('time')) + work.time)}""",
            "parse_mode": "Markdown",
        }
        response = requests.post(
            f"{TELEGRAM_URL}{company.company}/sendMessage", data=mess
        )

    to_day = datetime.datetime.now()


    for i in range(7):
        delta_day = to_day + datetime.timedelta(i)
    company = Company.objects.all()[0]
    works = Work.objects.filter(available=True)
        # Worker.objects.filter(day=)
    # works = Work.objects.all()
    # record_day =
    # print()
    return render(request, 'company/record.html', {'works': works, 'company': company})


def get_free_time(request, name, id):
    print(request.POST)
    # print(request.GET.get('text'))
    today = datetime.datetime.now().date()

    # calendar_day
    work = Work.objects.get(id=request.GET.get("work"))
    worker = Worker.objects.get(id=int(request.GET.get("worker")))
    days = worker.day.filter(date__range=[today, today + datetime.timedelta(7)])
    # for i in days:
    #     print(i.day)

    records = Record.objects.filter(worker=worker.id, day__range=[today, today + datetime.timedelta(7)])
    # for i in records:
    #     # print(i.user.name)

    response = {}
    res = {}
    for day in days:
        response[day.day] = []
        res[day.day] = []
        for time in range(worker.start_work, worker.end_work):
            for recorded in records:
                # print('Запис')
                # print(recorded.user.name, ' : ', create_time(recorded.start_time), ' - ', create_time(recorded.end_time))

                if recorded.start_time <= time <= recorded.end_time:
                    response[day.day].append(create_time(time))
                    # print('Запис час входить')

                    # print(day.day, ': ', create_time(recorded.start_time), create_time(time), create_time(recorded.end_time))
            ress = response
            if create_time(time) not in ress[day.day]:
                # print(ress[day.day])
                # print(create_time(time))
                res[day.day].append(create_time(time))

    # for i in res:
    #     res[i] = list(dict.fromkeys(res.get(i)))

    print('відповідь')
    print(res)
    # mylist = list(dict.fromkeys(mylist))
    # free_date = worker.day.filter()
    # users = User.objects.filter(phone_number__icontains='096')
    # # print(users[0].name)
    # res = []
    # for i in users:
    #     res.append({'name': i.name, 'id': i.id, 'phone': i.phone_number})
    # instance = Worker.objects.get(id=id)
    # art = instance.art
    # ip = instance.ip
    # instance.delete()
    # sum = 0
    # prod = Basket.objects.filter(ip=ip)
    # for i in prod:
    #     sum = sum + i.price
    # print(res)
    return JsonResponse(res)


# def site_form(re)


def find(request):
    status = request.user.is_authenticated
    if status:
        record = Record.objects.filter(company=request.user.last_name)
        return render(request, 'record_list.html', {"record": record})
    else:
        return redirect('login')


def dashboard(request):
    status = request.user.is_authenticated
    if status:
        try:
            day = datetime.datetime.now().date()
            workers = Worker.objects.filter(available=True, company=request.user.last_name)
            #filter(day=datetime.datetime.now().date())
            # records.order_by('time')
            # print(records[0].worker.id)
            kanban = {}
            l = []
            if request.GET.get('client') is not None:
                record = Record.objects.filter(company=request.user.last_name)
                data = {'client':request.GET.get('client')}

                if request.GET.get('from') is not None and request.GET.get('from') != '' and request.GET.get('to') is not None and request.GET.get('to') != '':
                    data['from'] = request.GET.get('from')
                    data['to'] = request.GET.get('to')
                    record = record.filter(day__range=[data.get('from'), data.get('to')])
                if request.GET.get('from') is not None:
                    data['from'] = request.GET.get('from')
                if request.GET.get('to') is not None:
                    data['to'] = request.GET.get('to')
                if request.GET.get('phone') is not None:
                    data['phone'] = request.GET.get('phone')
                if request.GET.get('name') is not None:
                    data['name'] = request.GET.get('name')
                if request.GET.get('work') is not None and request.GET.get('work') != '':
                    data['work'] = request.GET.get('work')
                if request.GET.get('worker') is not None and request.GET.get('worker') != '':
                    data['worker'] = request.GET.get('worker')
                for i in record:
                    print(i.day)
                    i.day = str(i.day).split(' ')[0]
                    i.start_time = create_time(i.start_time)

                # def home(request):
                #     posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')
                #     return render(request, 'frontend/home.html', {'posts': posts})
                if request.method == 'POST':
                    send_user(request.POST.get('user'), request.POST.get('header'), request.POST.get('text'), request)

                    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
                return render(request, 'record_list.html', {"record": record, 'data':data})
            else:
                if request.GET.get('date') is not None:
                    date = request.GET.get('date').split('-')
                    day = datetime.datetime(int(date[0]), int(date[1]), int(date[2]))
                    records = Record.objects.filter(available=True, company=request.user.last_name, day=day)
                else:
                    records = Record.objects.filter(available=True, company=request.user.last_name)
                for worker in workers:
                    list_time = []
                    record_list = []
                    n = 0
                    # print(records[0].day)
                    for time in range(12, 48):
                        day = datetime.datetime.now().date()
                        dont_here = True
                        for i in records:
                            if time == i.start_time:
                                # print(n)
                                # if n<=0:
                                #     n=n+i.end_time-i.start_time
                                print(n)
                                list_time.append({'time':create_time(i.start_time), 'price':i.work.price, "id": i.id, 'user': i.user.name, 'work': i.work.name, 'end': create_time(i.end_time), 'start': create_time(i.start_time), 'col':i.end_time-i.start_time})
                                dont_here = False
                                for i in range(i.start_time, i.end_time):
                                    record_list.append(i)
                                break
                        if dont_here == False:
                            continue
                        elif time not in record_list and worker.start_work <= time and worker.end_work >= time:
                            list_time.append({'time': create_time(time), 'record': True, "opt_time": time, "day": day, 'worker': worker.id})
                        elif time not in record_list:
                            list_time.append({'time': create_time(time), 'sleep': True})
                        else:
                            list_time.append({'time': create_time(time)})


                    l.append({'worker': worker.first_name, 'records': list_time})

                print(l)
                # r = [{"worker": 'Галина', 'records':[{'time':'12:00', 'user':'Oleg', 'col':4}, {'time':'12:30'}, {'time':'13:00'}, {'time':'13:30'}, {'time':'14:00', 'record':True}]}]
                # create_random_user_accounts.delay('total')
                return render(request, 'index.html', {"r": l, 'date': day.strftime('%Y-%m-%d')})
        except:
            return render(request, 'error.html')
    else:
        return redirect(login)


def api(request):
    date = datetime.datetime.now()
    date1 = datetime.datetime(date.year, date.month, date.day) + datetime.timedelta(1)
    date2 = date1 + datetime.timedelta(1)
    # analytic = {'5485423825:AAG8ro0zmiHk4FgZMxJ9o2iUjLqrBl7CRb4': 1, '54854238AG8ro0zmiHk4FgZMxJ9o2iUjLqrBl7CRb4': 1, '54854238AAG8ro0zmiHk4FgZMxJ9o2iUjLqrBl7CRb4': 1, '5485423825:': 1, }
    if date.hour == 20:
        records = Record.objects.filter(day__range=[date1, date2], notification_day=False).order_by('-chat_id')
    else:
        records = Record.objects.filter(day__range=[date1, date2], notification_hour=False).order_by('-chat_id')

    def send_mes(chat_id, work, time, token, text, record):
        data = {
            "chat_id": chat_id,
            "text": f'{text} {work} о *{time}*',
            "parse_mode": "Markdown",
        }
        response = requests.post(
            f"{TELEGRAM_URL}{token}/sendMessage", data=data
        )

        if response.status_code == 200:
            if 22 >= datetime.datetime.now().hour >= 20:
                Record.objects.filter(id=record).update(notification_day=True)
            elif 14 >= datetime.datetime.now().hour >= 12:
                Record.objects.filter(id=record).update(notification_hour=True)

    for record in records:
        if date.hour == 12:
            text = 'Добрий день, у вас завтра запланований запис на'
            send_mes(record.user.chat_id, record.work.name, create_time(record.start_time), record.company, text, record.id)
        if date.hour == 20:
            text = 'Добрий вечір, у вас завтра запланований запис на'
            send_mes(record.user.chat_id, record.work.name, create_time(record.start_time), record.company, text, record.id)

        # if analytic.get(str(record.company)+'_all') is not None:
        #     num = analytic.get(str(record.company)+'_all') + 1
        #     analytic[str(record.company)+'_all'] = num
        # else:
        #     analytic[str(record.company)+'_all'] = 1
    #     if response.status_code == 200:
    #         if analytic.get(record.company) is not None:
    #             num = analytic.get(record.company) + 1
    #             analytic[record.company] = num
    #         else:
    #             analytic[record.company] = 1
    # l=0
    # print(analytic)
    # for an in analytic:
    #
    #     print(an)


    return HttpResponse('hi')

def send_user(user, header, text, request):
    data = {
        "chat_id": user,
        "text": f'*{header}*\n{text}',
        "parse_mode": "Markdown",
    }
    response = requests.post(
        f"{TELEGRAM_URL}{request.user.last_name}/sendMessage", data=data
    )

def users(request):
    status = request.user.is_authenticated
    if status:
        try:
            if request.GET.get('client') is not None:
                users_company = User.objects.filter(Q(name__icontains=request.GET.get('client')) | Q(phone_number__icontains=request.GET.get('client')), company=request.user.last_name)
            elif request.POST.get('user') is not None and request.POST.get('header') is not None and request.POST.get('text') is not None:
                send_user(request.POST.get('user'), request.POST.get('header'), request.POST.get('text'), request)
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
            else:
                users_company = User.objects.filter(company=request.user.last_name)
            return render(request, 'users.html', {"users": users_company})
        except:
                return render(request, 'error.html')
    else:
        return redirect(login)


def user_detail(request, id):
    status = request.user.is_authenticated
    if status:
        try:
            if request.method == "POST":
                name = request.POST.get('name')
                phone = request.POST.get('phone')
                status = request.POST.get('status')
                date = request.POST.get('date').split('-')
                date = datetime.datetime(int(date[0]), int(date[1]), int(date[2]))
                comment = request.POST.get('descriptions')
                user = User.objects.filter(id=id, company=request.user.last_name).update(name=name, phone_number=phone, status=status, hapy_day=date, comment=comment)
                return redirect(users)
            else:
                client = User.objects.get(id=id, company=request.user.last_name)
                client.hapy_day = str(client.hapy_day).split(' ')[0]
                record = Record.objects.filter(company=request.user.last_name, user=client.id).order_by('-day', 'start_time')
                for i in record:
                    print(i.day)
                    i.day = str(i.day).split(' ')[0]
                    i.start_time = create_time(i.start_time)
                return render(request, 'user_detail.html', {"client": client, 'record': record})
        except:
                return render(request, 'error.html')
    else:
        return redirect(login)

#
# def call(request):
#     if request.GET.get('client') is not None:
#         users_company = Record.objects.filter(Q(name__icontains=request.GET.get('client')) | Q(phone_number__icontains=request.GET.get('client')))
#     else:
#         users_company = Record.objects.all()
#     return render(request, 'users.html', {"users": users_company})

def new_record(request):
    status = request.user.is_authenticated
    if status:
        try:
            if request.method == 'POST':
                print(request.POST)
                work = request.POST.get('pets')
                name = request.POST.get('name')
                user = request.POST.get('user_id')
                phone_number = request.POST.get('phoneNumber')
                time = create_system_time(request.POST.get('datetime').split('T')[1])
                day = request.POST.get('datetime').split('T')[0].split('-')
                day_db = datetime.datetime(int(day[0]), int(day[1]), int(day[2]))
                worker = request.POST.get('worker')
                comment = request.POST.get('note')
                if user is not None and user !='':
                    User.objects.filter(id=user).update(name=name)
                    user_db = User.objects.get(id=user)
                else:
                    user_db = User.objects.create(phone_number=phone_number, name=name)
                # try:
                work_time = Work.objects.get(id=work)
                worker_db = Worker.objects.get(id=worker)
                Record.objects.create(company=request.user.last_name, comment=comment, work=work_time, user=user_db, worker=worker_db, start_time=time, end_time=int(time)+work_time.time, day=day_db)
                # except:
                #     return redirect('dashboard')
                # Worker.objects.create(first_name=first_name, hapy_day=datetime.datetime.now(), user_name=username, last_name=last_name, phone_number=phone_number, company=request.user.last_name, chat_id=int(chat_id), img=img)
                return redirect('dashboard')
            work = Work.objects.filter(available=True, company=request.user.last_name)
            workers = Worker.objects.filter(available=True, company=request.user.last_name)
            return render(request, 'new_record.html', {'works': work, "workers": workers})
        except:
                return render(request, 'error.html')
    else:
        return redirect(login)


def create_record(request, day, time, worker):
    status = request.user.is_authenticated
    if status:
        try:
            if request.method == 'POST':

                # 'phoneNumber': ['+380963207695'], 'name': ['Віталій'], 'user_id': ['4'], 'pets': ['1'], 'note': [
                #     'jgk'], 'time': ['25'], 'day': ['2022-16-10'], 'worker': ['1'], 'csrfmiddlewaretoken': [
                #     'samPegCtvvQlUgg2HCeiYmjaD701S9x
                #     PJU2eckgPfwv2GPqMcCxt1Pk69Wqe8mEK']
                print(request.POST)
                work = request.POST.get('pets')
                name = request.POST.get('name')
                user = request.POST.get('user_id')
                phone_number = request.POST.get('phoneNumber')
                time = request.POST.get('time')
                # price = request.POST.get('price')
                day = request.POST.get('day').split('-')
                day_db = datetime.datetime(int(day[0]), int(day[1]), int(day[2]))
                worker = request.POST.get('worker')
                comment = request.POST.get('note')
                if user is not None and user !='':
                    User.objects.filter(id=user).update(name=name)
                    user_db = User.objects.get(id=user)
                else:
                    user_db = User.objects.create(phone_number=phone_number, name=name)
                # try:
                work_time = Work.objects.get(id=work)
                worker_db = Worker.objects.get(id=worker)
                Record.objects.create(company=request.user.last_name, comment=comment, work=work_time, user=user_db, worker=worker_db, start_time=time, end_time=int(time)+work_time.time, day=day_db)
                # except:
                #     return redirect('dashboard')
                # Worker.objects.create(first_name=first_name, hapy_day=datetime.datetime.now(), user_name=username, last_name=last_name, phone_number=phone_number, company=request.user.last_name, chat_id=int(chat_id), img=img)
                return redirect('dashboard')
            work = Work.objects.filter(available=True, company=request.user.last_name)
            return render(request, 'record.html', {'work': work, 'day':day, 'time':time, 'worker':worker})
        except:
                return render(request, 'error.html')
    else:
        return redirect(login)


def up_record(request, id):
    status = request.user.is_authenticated
    if status:
        try:
            if request.method == 'POST':
                print(request.POST)
                work = request.POST.get('pets')
                name = request.POST.get('name')
                user = request.POST.get('user_id')
                phone_number = request.POST.get('phoneNumber')
                time = create_system_time(request.POST.get('datetime').split('T')[1])
                day = request.POST.get('datetime').split('T')[0].split('-')
                day_db = datetime.datetime(int(day[0]), int(day[1]), int(day[2]))
                worker = request.POST.get('worker')
                comment = request.POST.get('note')
                if user is not None and user != '':
                    User.objects.filter(id=user).update(name=name)
                    user_db = User.objects.get(id=user)
                else:
                    user_db = User.objects.create(phone_number=phone_number, name=name)
                # try:
                work_time = Work.objects.get(id=work)
                worker_db = Worker.objects.get(id=worker)
                Record.objects.filter(id=id).update(company=request.user.last_name, comment=comment, work=work_time, user=user_db, worker=worker_db,
                                      start_time=time, end_time=int(time) + work_time.time, day=day_db)
                # except:
                #     return render(request, 'tables-basic.html')
                # Worker.objects.create(first_name=first_name, hapy_day=datetime.datetime.now(), user_name=username, last_name=last_name, phone_number=phone_number, company=request.user.last_name, chat_id=int(chat_id), img=img)
                return redirect('dashboard')
            work = Work.objects.filter(available=True, company=request.user.last_name)
            record = Record.objects.filter(company=request.user.last_name, id=id)[0]
            record.start_time = create_time(record.start_time)
            record.day = str(record.day.strftime('%Y-%m-%d'))
            print(record.day)
            workers = Worker.objects.filter(company=request.user.last_name, available=True)

            return render(request, 'update_record.html', {"record":record, "works": work, "workers": workers})
        except:
                return render(request, 'error.html')
    else:
        return redirect(login)

@csrf_protect
def delete_record(request):
    record = Record.objects.filter(id=request.POST.get('id'), company=request.user.last_name)
    if record is not None or list(record) != []:
        record.update(available=False)
        print(request.POST, request.user.last_name,)
        return JsonResponse({"status":200})
    else:
        return JsonResponse({"status": 404})


def get_inf(request):
    print(request.GET)
    print(request.GET.get('text'))
    users = User.objects.filter(phone_number__icontains=request.GET.get('text'), company=request.user.last_name, available=True)
    print(users[0].name)
    res = []
    for i in users:
        res.append({'name':i.name, 'id': i.id, 'phone': i.phone_number, 'comment': i.comment})
    # instance = Worker.objects.get(id=id)
    # art = instance.art
    # ip = instance.ip
    # instance.delete()
    # sum = 0
    # prod = Basket.objects.filter(ip=ip)
    # for i in prod:
    #     sum = sum + i.price
    print(res)
    return JsonResponse({'users':res[:5]})


def download_csv(request, queryset):
    opts = queryset.model._meta
    model = queryset.model
    response = HttpResponse(content_type='text/csv')
    # force download.
    response['Content-Disposition'] = 'attachment;filename=export.csv'
    # the csv writer
    writer = csv.writer(response)
    field_names = [field.name for field in opts.fields]
    # Write a first row with header information
    writer.writerow(field_names)
    # Write data rows
    for obj in queryset:
        writer.writerow([getattr(obj, field) for field in field_names])
    return response
download_csv.short_description = "Download selected as csv"


def exsport_users(request):
    data = download_csv(request, User.objects.all())
    return HttpResponse(data, content_type='text/csv')


def calendar(request):
    status = request.user.is_authenticated
    if status:
        try:
            if request.method == "POST":
                days = {}
                print(request.POST)
                for i in request.POST:

                    work_day = list(i.split("_"))
                    if days.get(work_day[0]) is not None:
                        days[work_day[0]].append(work_day[1])
                    else:
                        days[work_day[0]] = []
                for i in request.POST:
                    try:
                        work_day = list(i.split("_"))
                        days[work_day[0]].append(work_day[1])
                        day = Calendar.objects.get(day=work_day[1])
                        Worker.objects.get(id=int(work_day[0]), company=request.user.last_name).day.add(day.id)
                    except:
                        pass
                for user in days.keys():
                    try:
                        for i in list(Worker.objects.get(id=user, company=request.user.last_name).day.all().values_list('day')):
                            print(str(list(i)[0]), days[user])
                            if str(list(i)[0]) not in days[user]:
                                try:
                                    day = Calendar.objects.get(day=str(datetime.datetime.strptime(str(list(i)[0]), '%Y-%m-%d'))[:10])
                                    Worker.objects.get(id=user, company=request.user.last_name).day.remove(day.id)
                                except:
                                    print('pizda')
                    except:
                        print('pizda1')
            workers = Worker.objects.filter(company=request.user.last_name)
            worker = []
            for i in workers:
                days = i.day.all().values_list('day')
                i = model_to_dict(i)
                i['day'] = []
                for n in days:
                    i['day'].append(list(n)[0])
                worker.append(i)
            day = Calendar.objects.get(day=datetime.datetime.now().strftime('%Y-%m-%d'))
            days = Calendar.objects.filter(id__range=[day.id, day.id+30])
            return render(request, 'calendar.html', {'worker': worker, 'days': days})
        except:
                return render(request, 'error.html')
    else:
        return redirect('login')

# def create_calendar():
#     # %Y-%m-%d
#     for i in range(100):
#         day = datetime.datetime.now() + datetime.timedelta(days=i)
#         # Calendar.objects.get_or_create(day=day.strftime('%Y-%m-%d'))
#         Calendar.objects.filter(day=day.strftime('%Y-%m-%d')).update(date=day)
#         print('ok')
# create_calendar()



def worker(request):
    status = request.user.is_authenticated
    if status:
        try:
            if request.method == 'POST':
                first_name = request.POST.get('firstName')
                last_name = request.POST.get('lastName')
                username = request.POST.get('username')
                email = request.POST.get('email')
                phone_number = request.POST.get('phoneNumber')
                token = request.POST.get('token')
                chat_id = request.POST.get('chat_id')
                img = request.POST.get('photo')

                Worker.objects.create(first_name=first_name, hapy_day=datetime.datetime.now(), user_name=username, last_name=last_name, phone_number=phone_number, company=request.user.last_name, chat_id=int(chat_id), img=img)
                return redirect('workers')
            return render(request, 'pages-account-settings-account.html')
        except:
                return render(request, 'error.html')
    else:
        return redirect('login')

def create_system_time(time):
    str_time = str(time)[:5]
    our_time = int(str_time[:2])*2
    if str_time[3:] != '00':
        our_time = our_time + 1
    return our_time

def update_workers(request):
    status = request.user.is_authenticated
    if status:
        try:
            if request.method == 'POST':
                print(request.POST)
                for i in range(0, len(request.POST.getlist('chat_id'))):
                    work_start = request.POST.getlist('start_work')[i]
                    end_work = request.POST.getlist('end_work')[i]
                    print(f'available_{request.POST.getlist("chat_id")[i]}')
                    if request.POST.get(f'available_{request.POST.getlist("chat_id")[i]}') is not None:
                        available = True
                    else:
                        available = False
                    print(available)
                    Worker.objects.filter(chat_id=request.POST.getlist('chat_id')[i], company=request.user.last_name).update(first_name=request.POST.getlist('first_name')[i],  available=available, last_name=request.POST.getlist('last_name')[i], phone_number=request.POST.getlist('phone_number')[i], start_work=create_system_time(work_start), end_work=create_system_time(end_work))
            return redirect('workers')
        except:
                return render(request, 'error.html')
    else:
        return redirect('login')


def up_work(request, id):
    status = request.user.is_authenticated
    if status:
        try:
            if request.method == 'POST':
                print(request.POST)
                name = request.POST.get('name')
                price = request.POST.get('price')
                time = request.POST.get('time')
                descriptions = request.POST.get('descriptions')
                work = Work.objects.filter(id=id)[0]
                work.worker.clear()
                for i in list(request.POST.getlist('worker')):
                    print(i)
                    if i != '':
                        worker = Worker.objects.get(chat_id=i, company=request.user.last_name)
                        work.worker.add(worker.id)
                if request.POST.get("available") is not None:
                    available = True
                else:
                    available = False
                Work.objects.filter(id=id).update(name=name, price=int(price), time=int(time), descriptions=descriptions, available=available,
                                    img='https://www.elegantweddinginvites.com/wedding-blog/wp-content/uploads/2020/07/beautiful-bridal-nails-for-your-wedding-look.jpg',)
            else:
                work = Work.objects.filter(id=id)[0]
                print(work.name)
                workers = Worker.objects.filter(company=request.user.last_name)
                return render(request, 'update_work.html', {"work": work, 'workers': workers})
            return redirect('works')
        except:
                return render(request, 'error.html')
    else:
        return redirect('login')


def work(request):
    status = request.user.is_authenticated
    if status:
        try:
            if request.method == 'POST':
                print(request.POST)
                name = request.POST.get('name')
                price = request.POST.get('price')
                time = request.POST.get('time')
                img = request.POST.get('img')
                descriptions = request.POST.get('descriptions')
                if request.POST.get('interval') is not None:
                    interval = request.POST.get('interval')
                    work = Work.objects.create(name=name, price=int(price), interval=int(interval), time=int(time), descriptions=descriptions,
                                        img=img,
                                        company=request.user.last_name)
                else:
                    work = Work.objects.create(name=name, price=int(price), time=int(time), descriptions=descriptions,
                                        img=img,
                                        company=request.user.last_name)

                for i in list(request.POST.getlist('worker')):
                    print(i)
                    worker = Worker.objects.get(chat_id=i, company=request.user.last_name)
                    work.worker.add(worker.id)
                return redirect('works')
            workers = Worker.objects.filter(company=request.user.last_name)
            return render(request, 'form-layouts-vertical.html', {"workers":workers})
        except:
                return render(request, 'error.html')
    else:
        return redirect('login')

def works(request):
    status = request.user.is_authenticated
    if status:
        try:
            work = Work.objects.filter(company=request.user.last_name)
            for i in work:
                i.time = create_time(i.time)
            return render(request, 'works_table.html', {"works": work})
        except:
                return render(request, 'error.html')
    else:
        return redirect('login')

#
# def my_view(request):
#     ...
#     return redirect('some-view-name', foo='bar')

def workers(request):
    status = request.user.is_authenticated
    if status:
        try:
            workers = Worker.objects.filter(company=request.user.last_name)
            for i in workers:
                i.start_work = create_time(i.start_work)
                i.end_work = create_time(i.end_work)
            return render(request, 'tables-basic.html', {"workers":workers})
        except:
                return render(request, 'error.html')
    else:
        return redirect('login')

class RegisterBotView(View):
    # poebalu
    def post(self, request, token, *args, **kwargs):
        message_type = json.loads(request.body)
        import telebot
        bot = telebot.TeleBot(token)
        # try:
        handler = message_type['message']
        t_chat = handler["chat"]['id']
        if handler.get('text') is not None:
            text = handler.get('text')
            if text == '/start':
                print(0)
                bot.send_message(t_chat, 'Зареєструй свого уєбана - напиши токен!')
            elif text is not None:
                requests.get(f"https://api.telegram.org/bot{text}/setWebhook?url=https://831d-109-108-245-24.eu.ngrok.io/webhooks/tutorial/{text}")
                bot.send_message(t_chat, 'Якщо це був токен, то твій бот працює, інакше ти дура')
            else:
                bot.send_message(t_chat, 'Таку хуйню мені ше не скидали, я передам розрабам!')
        print(1)
        # except:
        #     pass
        return JsonResponse({"ok": "POST request processed"})


# function create time
def create_time(i):
    if i % 2 == 1:
        if len(str(i / 2)) == 3:
            hour = '0' + str(i / 2)[:-2] + ':30'
        else:
            hour = str(i / 2)[:-2] + ':30'
    else:
        if len(str(i / 2)) == 3:
            hour = '0' + str(i / 2)[:-2] + ':00'
        else:
            hour = str(i / 2)[:-2] + ':00'
    return hour


def notifications(request):
    status = request.user.is_authenticated
    if status:
        if request.method == 'POST':
            name = request.POST.get('name')
            photo = request.POST.get('photo')
            token = request.user.last_name
            descriptions = request.POST.get('descriptions')
            chat_id = request.POST.get('chat_id')
            if chat_id is None or chat_id != '':
                data = {
                    "chat_id": chat_id,
                    "caption": f"*{name}*\n{descriptions}",
                    "photo":photo,
                    "parse_mode": "Markdown",
                }
                response = requests.post(
                    f"{TELEGRAM_URL}{token}/sendPhoto", data=data
                )
            else:
                users = User.objects.filter(company=request.user.last_name)
                for i in users:
                    data = {
                        "chat_id": i.chat_id,
                        "caption": f"*{name}*\n{descriptions}",
                        "photo": photo,
                        "parse_mode": "Markdown",
                    }
                    response = requests.post(
                        f"{TELEGRAM_URL}{request.user.last_name}/sendPhoto", data=data
                    )
                    print(response.json())
            return redirect('dashboard')
        return render(request, 'notifications.html')
    else:
        return redirect('login')

def next_day(day, func):
    print(day)
    d = datetime.datetime.strptime(str(day), '%Y-%m-%d')
    print(d)
    if func == 'next':
        day = d + datetime.timedelta(days=1)
    else:
        day = d - datetime.timedelta(days=1)
    return day.strftime('%Y-%m-%d')


def get_work_days(worker, company):
    today = datetime.datetime.now()
    # worker.day.filter(date__range=[today, today + datetime.timedelta(7)])
    days = Worker.objects.get(chat_id=worker).day.filter(date__range=[today, today + datetime.timedelta(7)])
    return days


# @sync_to_async
# def notifications():
#     while True:
#         # now = datetime.datetime.now()
#         # dt = '2022-26-09'
#         # day = dt.split('-')
#         # dt_str = f'{day[0]}-{int(day[1])-1}-{day[2]}'
#         #
#         # print(now.strftime("%Y-%m-%d"))
#         # dt_obj = datetime.datetime.strptime(dt_str, '%Y-%d-%m')
#         # print(str(dt_obj)[:10])
#         # if str(dt_obj)[:10] == now.strftime("%Y-%m-%d"):
#         #     print('ok')

#
#         day = datetime.datetime.now() + datetime.timedelta(days=1)
#         print(day.strftime('%Y-%d-%m'))
#         record = Record.objects.filter(day=str(day.strftime('%Y-%d-%m')))
#         print(record)
#         # for i in
#         print('11')
#         # 11 hour = 39600
#         sleep(1)
#
# async_function = sync_to_async(notifications, thread_sensitive=False)

# def simple():
#     print('1')
#     print('2022-10-02' in Worker.objects.get(id=1).day.values_list('day' , flat=True))
#     print('2')
# simple()
quest = {}
user = {}
class TutorialBotView(View):
    list_display = ['name', 'chat_id', 'start_time', 'end_time', 'day', 'company']


    def post(self, request, token, *args, **kwargs):
        message_type = json.loads(request.body)
        print(message_type)

        #
# {'update_id': 968143463, 'edited_message': {'message_id': 5898, 'from': {'id': 537238295, 'is_bot': False, 'first_name': 'Віталій', 'username': 'gritsulevich', 'language_code': 'en'}, 'chat': {'id': 537238295, 'first_name': 'Вітал
# ій', 'username': 'gritsulevich', 'type': 'private'}, 'date': 1664902187, 'edit_date': 1664902285, 'location': {'latitude': 49.232439, 'longitude': 28.431204, 'live_period': 900, 'heading': 239, 'horizontal_accuracy': 19.0}}}

        #
# {'update_id': 968143464, 'edited_message': {'message_id': 5898, 'from': {'id': 537238295, 'is_bot': False, 'first_name': 'Віталій', 'username': 'gritsulevich', 'language_code': 'en'}, 'chat': {'id': 537238295, 'first_name': 'Вітал
# ій', 'username': 'gritsulevich', 'type': 'private'}, 'date': 1664902187, 'edit_date': 1664902316, 'location': {'latitude': 49.232441, 'longitude': 28.431123, 'live_period': 900, 'heading': 181, 'horizontal_accuracy': 23.0}}}
        #
        #
# {'update_id': 968143462, 'edited_message': {'message_id': 5898, 'from': {'id': 537238295, 'is_bot': False, 'first_name': 'Віталій', 'username': 'gritsulevich', 'language_code': 'en'}, 'chat': {'id': 537238295, 'first_name': 'Вітал
# ій', 'username': 'gritsulevich', 'type': 'private'}, 'date': 1664902187, 'edit_date': 1664902254, 'location': {'latitude': 49.232483, 'longitude': 28.431157, 'live_period': 900, 'heading': 296, 'horizontal_accuracy': 18.0}}}
        #
        #
# {'update_id': 968143464, 'edited_message': {'message_id': 5898, 'from': {'id': 537238295, 'is_bot': False, 'first_name': 'Віталій', 'username': 'gritsulevich', 'language_code': 'en'}, 'chat': {'id': 537238295, 'first_name': 'Вітал
# ій', 'username': 'gritsulevich', 'type': 'private'}, 'date': 1664902187, 'edit_date': 1664902316, 'location': {'latitude': 49.232441, 'longitude': 28.431123, 'live_period': 900, 'heading': 181, 'horizontal_accuracy': 23.0}}}
        #
# {'update_id': 968143473, 'message': {'message_id': 5903, 'from': {'id': 537238295, 'is_bot': False, 'first_name': 'Віталій', 'username': 'gritsulevich', 'language_code': 'uk'}, 'chat': {'id': 537238295, 'first_name': 'Віталій', 'u
# sername': 'gritsulevich', 'type': 'private'}, 'date': 1664903832, 'contact': {'phone_number': '380963207695', 'first_name': 'Віталій', 'user_id': 537238295}}}


        import telebot
        bot = telebot.TeleBot(token)

        def next_step(text, t_chat):
            keyboard = types.InlineKeyboardMarkup()
            but1 = types.InlineKeyboardButton(text=f"Календар", callback_data="cbcal_0_s_y_2022_9_12")
            bot.send_message(t_chat, f"Відкрити календар", reply_markup=keyboard.add(but1))

        # handler
        try:
            handler = message_type['message']
            t_chat = handler["chat"]['id']
            # next_step_user[t_chat] = {'status': False,
            #                           'func': None}
            if handler.get('text') is not None:
                text = handler.get('text')
                # Опитування

                def close_next_step(text):
                    if text[:1] == '/' or text == 'close':
                        next_step_user[t_chat] = {'status': False,
                                                  'func': None}
                        bot.send_message(t_chat, f"Опитування завершено, дані стерто")
                        return next_step_user

                if text == '/start':
                    try:
                        User.objects.get(chat_id=t_chat, company=token)
                        keyboard = types.InlineKeyboardMarkup()
                        but1 = types.InlineKeyboardButton(text=f"Записатись", callback_data="cbcal_0_s_y_2022_9_12")
                        bot.send_message(t_chat, f"Записатись", reply_markup=keyboard.add(but1))

                        # (chat_id=t_chat, company=token, name=handler['from'].get('first_name'), user_name=handler['from'].get('user_name'), hapy_day='2022-01-01')


                    except:
                        User.objects.create(chat_id=t_chat, company=token, name=handler['from'].get('first_name'), user_name=handler['from'].get('user_name'),
                                         hapy_day='2022-01-01')
                        keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
                        button_phone = types.KeyboardButton(text="Отправить номер телефона", request_contact=True)
                        bot.send_message(t_chat, f"Вітаємо тебе в телеграм, переваги:\n\n - Персональні знижки!\n - Перевагу у записі!\n - Нагадування про запис!\n - Історію записів!\n - Акційні пропозиції!\n - Дисконтну карту!\n - Та вільні віконечка)\n\nХучіш реєструйся - тисни кнопку.", reply_markup=keyboard.add(button_phone))

                elif text == '/record':
                    keyboard = types.InlineKeyboardMarkup()
                    for i in Work.objects.filter(company=token, available=True):
                        but1 = types.InlineKeyboardButton(text=f"{i.name}", callback_data=f"work_{i.id}")
                        keyboard.add(but1)
                    bot.send_message(t_chat, 'На яку процедуру?', reply_markup=keyboard)
                elif text == '/quest':
                    keyboard = types.InlineKeyboardMarkup()
                    public = types.InlineKeyboardButton(text=f"Я не соромлюсь", callback_data=f"quest_public")
                    anonym = types.InlineKeyboardButton(text=f"Анонімна порада", callback_data=f"quest_anonym")
                    keyboard.add(public, anonym)
                    bot.send_message(t_chat, 'Бажаєте щоб *бачили автора* листа?\nЧи віддаєте перевагу *анонімному*?', reply_markup=keyboard, parse_mode='markdown')

                elif text == '/my_record':
                    text = f"*Записи на сьогодні* {datetime.datetime.today().date()}"
                    for i in Record.objects.filter(company=token, worker=t_chat,day=str(datetime.datetime.today().date())):
                        text = text + f'\n*{create_time(i.start_time)}* - {create_time(i.end_time)} {i.work.name}'
                        if i.available:
                            text = text + ' ✅'
                        else:
                            text = text + ' ❌'
                    keyboard = types.InlineKeyboardMarkup()
                    but2 = types.InlineKeyboardButton(text=f"попередній", callback_data=f"work_next_{next_day(datetime.datetime.today().date(), 'pre')}")
                    but1 = types.InlineKeyboardButton(text=f"наступний", callback_data=f"work_next_{next_day(datetime.datetime.today().date(), 'next')}")
                    bot.send_message(t_chat, text, reply_markup=keyboard.add(but2, but1), parse_mode='markdown')







                elif text == '/get_analytics':
                    # if message.chat.id in admin:
                    workbook = xlwt.Workbook()
                    sheet = workbook.add_sheet("contacts")#, cell_overwrite_ok=True
                    header_font = xlwt.Font()
                    header_font.name = 'Arial'
                    header_font.bold = True
                    header_style = xlwt.XFStyle()
                    header_style.font = header_font
                    list_header = ['day', 'name', 'phone', 'chat_id', 'address', 'payment', 'product', 'price',
                                   'num', 'summ of product', 'hours', 'summ']
                    for i in range(0, 11):
                        sheet.write(0, i, list_header[i], header_style)

                    res = Record.objects.filter(company=token)
                    print(res[0].company)
                    n = 0
                    row = 1
                    for i in res:
                        sheet.write(row, 1, str(i.user.name))
                        sheet.write(row, 2, str(i.company))
                        sheet.write(row, 3, str(i.worker.first_name))
                        n = n + 1
                        row = row + 1
                    # for i in range(0, 11):
                    #     sheet.write(0, i, list_header[i], header_style)
                    # data = dict(json.load(open('data.json', 'r', encoding='utf-8')))
                    # n = 0
                    # row = 1
                    # for i in data.keys():
                    #     user = data.get(i)
                    #     for item in user:
                    #         summ = 0
                    #         key = list(item.get('order').keys())[0]
                    #         for i in item.get('order').get(key):
                    #             summ = summ + (int(i.get('price')) * int(i.get('num')))
                    #         for i in item.get('order').get(key):
                    #             summ_of_prod = int(i.get('num')) * int(i.get('price'))
                    #             sheet.write(row, 0, str(item.get('time').split(' ')[0].replace('-', '.')),
                    #                         header_style)
                    #             sheet.write(row, 1, item.get('name'), header_style)
                    #             sheet.write(row, 2, item.get('phone'), header_style)
                    #             sheet.write(row, 3, str(key), header_style)
                    #             sheet.write(row, 4, item.get('address'), header_style)
                    #             sheet.write(row, 5, item.get('payment'), header_style)
                    #             sheet.write(row, 6, i.get('product'), header_style)
                    #             sheet.write(row, 7, i.get('price'), header_style)
                    #             sheet.write(row, 8, i.get('num'), header_style)
                    #             sheet.write(row, 9, summ_of_prod, header_style)
                    #             sheet.write(row, 10, str(item.get('time').split(' ')[1][:5]), header_style)
                    #             sheet.write(row, 11, summ, header_style)
                    #             n = n + 1
                    #             row = row + 1
                    workbook.save('info_orders.xls')
                    print('it`s ok')
                    bot.send_document(t_chat, document=open("./info_orders.xls", "rb"),
                                      visible_file_name=f'info_orders_{str(datetime.datetime.now().strftime("%Y-%m-%d"))}.xls',
                                      caption="Аналітика")













                elif text == '/works':
                    keyboard = types.InlineKeyboardMarkup()
                    text = 'Наші *послуги:*\n'
                    for i in Work.objects.filter(available=True, company=token):
                        text = text + f'\n*{i.name}*\n\n*Тривалість:* {create_time(i.time)} год.\n*Ціна:* {i.price} грн.\n*Опис:*{i.descriptions}\n*------------------*'
                        but1 = types.InlineKeyboardButton(text=f"{i.name}", callback_data=f"work_{i.id}")
                        keyboard.add(but1)
                    text = text + '\n*записуйся нижче:*'
                    bot.send_message(t_chat, text, reply_markup=keyboard, parse_mode='markdown')
                elif text == '/about':
                    company = Company.objects.all()[0]
                    about = f'*{company.name}*\n\n{company.mini_descriptions}\n\n*Мобільний:* {company.phone_number}\n*Адреса:* {company.address}\n\n*© {datetime.datetime.now().year} created* by *ViForce* ❤️‍🔥'
                    data = {
                        "chat_id": t_chat,
                        "caption": about,
                        "photo": company.banner,
                        "parse_mode": "Markdown",
                    }
                    response = requests.post(
                        f"{TELEGRAM_URL}{token}/sendPhoto", data=data
                    )

                elif text == '/profile':
                    records = Record.objects.filter(user__chat_id=t_chat, company=token, available=True)
                    keyboard = types.InlineKeyboardMarkup()
                    send_text = '*Ваші записи:*\n'
                    zero = True
                    for i in records.filter(day__range=[datetime.datetime.now()-datetime.timedelta(30), datetime.datetime.now()+datetime.timedelta(7)]):
                        # if datetime.datetime.strptime(i.day, '%Y-%m-%d') >= datetime.datetime.now() :
                        zero = False
                        send_text = send_text + f'\n*{i.day.strftime("%d.%m.%Y")}* {create_time(i.start_time)}, {i.work.name}, *{i.worker.first_name}*\n'
                    but1 = types.InlineKeyboardButton(text=f"Скасувати", callback_data=f"close_record")
                    keyboard.add(but1)
                    if zero:
                        bot.send_message(t_chat, f'Найближчим часом записів немає, зміни це, тисни /record')
                    else:
                        bot.send_message(t_chat, f'{send_text}\nСказувати якийсь запис?', reply_markup=keyboard, parse_mode="markdown")
                elif quest.get(t_chat) is not None:
                    if quest.get(t_chat)[0] == True:
                        admin = Company.objects.get(company=token).chat_id
                        user_quest = User.objects.get(chat_id=t_chat)
                        if quest.get(t_chat)[1] == 'public':
                            quest.pop(t_chat)
                            bot.send_message(admin, f'*Повідомлення з бота:*\n{text}\n*Автор:* {user_quest.name} - {user_quest.phone_number}, {user_quest.user_name}', parse_mode="markdown")
                            bot.send_message(t_chat, f'Готово, адмін отримав твій лист!', parse_mode="markdown")
                        else:
                            quest.pop(t_chat)
                            bot.send_message(admin,
                                             f'*Повідомлення з бота:*\n{text}\n*Автор:* таємний!', parse_mode="markdown")

                            bot.send_message(t_chat, f'Готово, адмін отримав твій лист!', parse_mode="markdown")

                    else:
                        quest.pop(t_chat)
                        bot.send_message(t_chat, f'Дивно, не відправилось!', parse_mode="markdown")

                else:
                    if next_step_user[t_chat].get('status') is True:
                        close_next_step(text)
                        next_step_user[t_chat]['func'](text, t_chat)

            elif handler.get('contact') is not None:
                User.objects.filter(chat_id=t_chat).update(phone_number=str(handler.get('contact').get('phone_number'))[2:])
                keyboard = types.InlineKeyboardMarkup()
                for i in Work.objects.filter(company=token, available=True):
                    but1 = types.InlineKeyboardButton(text=f"{i.name}", callback_data=f"work_{i.id}")
                    keyboard.add(but1)
                bot.send_message(t_chat, 'Дякую за довіру, вас зареєсторовано :)\n\nНа яку процедуру хочеш?', reply_markup=keyboard)

            elif handler.get('photo') is not None:




                bot.send_message(t_chat, 'photo')
            elif handler.get('voice') is not None:




                bot.send_message(t_chat, 'voice')
            elif handler.get('document') is not None:




                bot.send_message(t_chat, 'file')
            else:
                bot.send_message(t_chat, 'Таку хуйню мені ше не скидали, я передам розрабам!')
        except:
            # Я отримую cbcal_0_s_m_2022_5_12 треба визначити чи майстер вільний
            t_chat = message_type['callback_query']['from'].get('id')
            message_id = message_type['callback_query']['message'].get('message_id')
            try:
                result, key, step = DetailedTelegramCalendar(locale='ru').process(message_type['callback_query']['data'])
                if not result and key:
                    bot.edit_message_text(f"Select {LSTEP[step]}",
                                          t_chat,
                                          message_id,
                                          reply_markup=key)
                elif result:
                    keyboard = types.InlineKeyboardMarkup()
                    buttons = []
                    user['date'] = result
                    record_work = Record.objects.filter(company=token, worker=Worker.objects.get(chat_id=t_chat), day=user['date']).values_list('start_time', 'end_time')
                    print(record_work)
                    recording_work = []
                    for i in record_work:
                        for n in range(list(i)[0], list(i)[1]):
                            recording_work.append(n)
                    print(recording_work)
                    if user['date'] in Worker.objects.get(chat_id=user['worker']).day.values_list('day', flat=True):
                        for i in range(18, 40):

                            if i % 2 == 1:
                                if len(str(i / 2)) == 3:
                                    hour = '0' + str(i / 2)[:-2] + ':30'
                                else:
                                    hour = str(i / 2)[:-2] + ':30'
                            else:
                                if len(str(i / 2)) == 3:
                                    hour = '0' + str(i / 2)[:-2] + ':00'
                                else:
                                    hour = str(i / 2)[:-2] + ':00'
                            if i in recording_work:
                                but1 = types.InlineKeyboardButton(text=f"{hour}❌", callback_data="close")
                            else:
                                but1 = types.InlineKeyboardButton(text=f"{hour}✅", callback_data=f"time{i}")
                            buttons.append(but1)
                    else:
                        for i in range(18, 40):

                            if i % 2 == 1:
                                if len(str(i / 2)) == 3:
                                    hour = '0' + str(i / 2)[:-2] + ':30'
                                else:
                                    hour = str(i / 2)[:-2] + ':30'
                            else:
                                if len(str(i / 2)) == 3:
                                    hour = '0' + str(i / 2)[:-2] + ':00'
                                else:
                                    hour = str(i / 2)[:-2] + ':00'
                            but1 = types.InlineKeyboardButton(text=f"{hour}❌", callback_data="close")
                            buttons.append(but1)

                    for i in range(0, len(buttons), 2):
                        keyboard.add(buttons[i], buttons[i + 1])
                    print('llll')

                    but = types.InlineKeyboardButton(text=f"Попередній день", callback_data=f"cbcal_0_s_y_{next_day(user['date'], 'pre')}")
                    but1 = types.InlineKeyboardButton(text=f"Наступний день", callback_data=f"cbcal_0_s_y_{next_day(user['date'], 'next')}")
                    keyboard.add(but, but1)
                    print('kkkk')
                    bot.edit_message_text(f"День {user['date']}, обирай час:", t_chat, message_id, reply_markup=keyboard)
                    print('mmmm')

            except:
                if message_type['callback_query']['data'][:4] == 'time':
                    user['time'] = int(message_type['callback_query']['data'][4:])
                    keyboard = types.InlineKeyboardMarkup()
                    but1 = types.InlineKeyboardButton(text=f"Записатись", callback_data=f"record")
                    but2 = types.InlineKeyboardButton(text=f"Скасувати", callback_data=f"edit")
                    bot.edit_message_text(f"Записую вас до майстра {Worker.objects.filter(chat_id=user.get('worker'))[0].first_name} на {Work.objects.get(id=user['work']).name} {user.get('date')} числа, о {create_time(user['time'])}?",
                                          t_chat, message_id, reply_markup=keyboard.add(but1, but2))
                elif message_type['callback_query']['data'][:10] == 'work_next_':
                    keyboard = types.InlineKeyboardMarkup()
                    date = message_type['callback_query']['data'][10:]
                    record_work = Record.objects.filter(company=token, day=date)
                    text = f"*Записи на* {date}\n"
                    for i in record_work:
                        text = text + f'\n*{create_time(i.start_time)}* - {create_time(i.end_time)} {i.work.name}'
                        if i.available:
                            text = text + ' ✅'
                        else:
                            text = text + ' ❌'
                    but2 = types.InlineKeyboardButton(text=f"попередній", callback_data=f"work_next_{next_day(date, 'pre')}")
                    but1 = types.InlineKeyboardButton(text=f"наступний", callback_data=f"work_next_{next_day(date, 'next')}")
                    bot.edit_message_text(text, t_chat, message_id, reply_markup=keyboard.add(but2, but1), parse_mode='markdown')

                # elif message_type['callback_query']['data'][:12] == 'cbcal_0_s_y_':
                elif message_type['callback_query']['data'][:12] == 'date_record_':
                    keyboard = types.InlineKeyboardMarkup()
                    buttons = []
                    data = str(message_type['callback_query']['data'][12:]).split('-')
                    user['date'] = datetime.datetime(int(data[0]), int(data[1]), int(data[2]))
                    record_work = Record.objects.filter(company=token, worker=Worker.objects.get(chat_id=user['worker']),
                                                        day__range=[user['date'], user['date'] + datetime.timedelta(1) ]).values_list('start_time', 'end_time')
                    print(record_work)
                    recording_work = []
                    for i in record_work:
                        for n in range(list(i)[0], list(i)[1]):
                            recording_work.append(n)
                    print(recording_work)
                    worker = Worker.objects.get(chat_id=user['worker'])
                    if worker.day.filter(date__range=[user['date'], user['date'] + datetime.timedelta(1)]) is not None:

                        for i in range(worker.start_work, worker.end_work):
                            if i % 2 == 1:
                                if len(str(i / 2)) == 3:
                                    hour = '0' + str(i / 2)[:-2] + ':30'
                                else:
                                    hour = str(i / 2)[:-2] + ':30'
                            else:
                                if len(str(i / 2)) == 3:
                                    hour = '0' + str(i / 2)[:-2] + ':00'
                                else:
                                    hour = str(i / 2)[:-2] + ':00'
                            if i in recording_work:
                                but1 = types.InlineKeyboardButton(text=f"{hour}❌", callback_data="close")
                            else:
                                but1 = types.InlineKeyboardButton(text=f"{hour}✅", callback_data=f"time{i}")
                            buttons.append(but1)
                    else:
                        for i in range(worker.start_work, worker.end_work):

                            if i % 2 == 1:
                                if len(str(i / 2)) == 3:
                                    hour = '0' + str(i / 2)[:-2] + ':30'
                                else:
                                    hour = str(i / 2)[:-2] + ':30'
                            else:
                                if len(str(i / 2)) == 3:
                                    hour = '0' + str(i / 2)[:-2] + ':00'
                                else:
                                    hour = str(i / 2)[:-2] + ':00'
                            but1 = types.InlineKeyboardButton(text=f"{hour}❌", callback_data="close")
                            buttons.append(but1)
                    for i in range(0, len(buttons), 2):
                        keyboard.add(buttons[i], buttons[i + 1])
                    print('llll')

                    but = types.InlineKeyboardButton(text=f"Попередній день",
                                                     callback_data=f"date_record_{next_day(str(user['date']).split(' ')[0], 'pre')}")
                    but1 = types.InlineKeyboardButton(text=f"Наступний день",
                                                      callback_data=f"date_record_{next_day(str(user['date']).split(' ')[0], 'next')}")
                    keyboard.add(but, but1)
                    print('kkkk')
                    bot.edit_message_text(f"День {user['date'].date()}, обирай час:", t_chat, message_id,
                                          reply_markup=keyboard)
                    print('mmmm')
                elif message_type['callback_query']['data'] == 'record':
                    worker = Worker.objects.get(chat_id=user['worker'])
                    print(worker)
                    work = Work.objects.get(id=user['work'])
                    worker = Worker.objects.get(chat_id=user['worker'])
                    date = user['date']
                    Record.objects.create(work=work, chat_id=t_chat, day=date, user=User.objects.get(chat_id=t_chat), worker=worker, start_time=int(user['time']), end_time=int(user['time'])+4, company=token)
                    # bot.send_message(user['worker'], f'{t_chat} хоче до тебе\n{user["date"]} о {create_time(user["time"])} на {Work.objects.get(id=user["work"]).name}')
                    bot.edit_message_text(f"Записали!", t_chat, message_id)

                elif message_type['callback_query']['data'] == 'quest_public':
                    quest[t_chat] = [True, 'public']
                    keyboard = types.InlineKeyboardMarkup()
                    cansel = types.InlineKeyboardButton(text=f"Скасувати",
                                                     callback_data=f"quest_cansel")
                    keyboard.add(cansel)
                    bot.edit_message_text(f"Пиши лист, все що ти зараз відправиш, я передам адміну та згадаю тебе, як автора))", t_chat, message_id, reply_markup=keyboard)

                elif message_type['callback_query']['data'] == 'quest_anonym':
                    quest[t_chat] = [True, 'anonym']
                    keyboard = types.InlineKeyboardMarkup()
                    cansel = types.InlineKeyboardButton(text=f"Скасувати",
                                                     callback_data=f"quest_cansel")
                    keyboard.add(cansel)
                    bot.edit_message_text(f"Опиши свою думку чітко, щоб адмін зрозумів, а я передам його анонімно, ні згадки про тебе))!", t_chat, message_id, reply_markup=keyboard)
                elif message_type['callback_query']['data'] == 'quest_anonym':
                    quest.pop(t_chat, None)

                elif message_type['callback_query']['data'] == 'edit':
                    bot.edit_message_text(f"Скасовано!", t_chat, message_id)

                elif message_type['callback_query']['data'][:6] == 'worker':
                    user['worker'] = int(message_type['callback_query']['data'][7:])
                    print('\n\n\n\n\n\n\n\n\n', user['worker'])
                    keyboard = types.InlineKeyboardMarkup()
                    # but1 = types.InlineKeyboardButton(text=f"Календар", callback_data="cbcal_0_s_y_2022_9_12")
                    days = get_work_days(user['worker'], token)
                    for i in days:
                        but1 = types.InlineKeyboardButton(text=f"{str(i.day).replace('-', '.')}", callback_data=f"date_record_{i.day}")
                        keyboard.add(but1)
                    bot.send_message(t_chat, "Обери дату:", reply_markup=keyboard)
                    bot.delete_message(t_chat,  message_id)
                elif message_type['callback_query']['data'] == 'close_record':
                    bot.send_message(t_chat, f"Напишіть нашому адміністратору @gritsulevich, він допоможе)")
                elif message_type['callback_query']['data'] == 'close':
                    bot.send_message(t_chat, f"В цей час майстер набирається сил, напишіть нашому адміністратору @gritsulevich")

                elif message_type['callback_query']['data'][:4] == 'work':
                    user['work'] = int(message_type['callback_query']['data'][5:])
                    keyboard = types.InlineKeyboardMarkup()
                    work = Work.objects.get(id=user['work'])
                    for i in work.worker.all():
                        but1 = types.InlineKeyboardButton(text=f"{i.first_name}", callback_data=f"worker_{i.chat_id}")
                        keyboard.add(but1)

                    but1 = types.InlineKeyboardButton(text=f"Назад", callback_data=f"cbcal_0_s_y_2022_9_12")
                    keyboard.add(but1)
                    bot.send_photo(t_chat, work.img, caption="Обери майстра:", reply_markup=keyboard)
                    bot.delete_message(t_chat, message_id)

        return JsonResponse({"ok": "POST request processed"})

    @staticmethod
    def send_message(message, chat_id):
        data = {
            "chat_id": chat_id,
            "text": message,
            "parse_mode": "Markdown",
        }
        response = requests.post(
            f"{TELEGRAM_URL}{TUTORIAL_BOT_TOKEN}/sendMessage", data=data
        )
